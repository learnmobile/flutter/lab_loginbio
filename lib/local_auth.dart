import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';
import 'package:local_auth_android/local_auth_android.dart';

class LocalAuth {
  static final _auth = LocalAuthentication();

  // device capabilities
  static Future<bool> _canAuthenticate() async {
    return await _auth.canCheckBiometrics || await _auth.isDeviceSupported();
  }

  static Future<bool> didAuthenticate() async {
    try {
      if (!await _canAuthenticate()) {
        return false;
      }
      return await _auth.authenticate(
          authMessages: const <AuthMessages>[
            AndroidAuthMessages(
              signInTitle: 'Oh! Biometric auth required!',
              cancelButton: 'No thanks',
            ),
          ],
          localizedReason: 'Please authenticate',
          options: const AuthenticationOptions(
              useErrorDialogs: true, stickyAuth: true));
    } catch (e) {
      debugPrint('$e');
      return false;
    }
  }
}
