import 'package:flutter/material.dart';
import 'main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/get.dart';

class Habilitation extends StatefulWidget {
  const Habilitation({super.key});

  @override
  State<Habilitation> createState() => _Habilitation();
}

class _Habilitation extends State<Habilitation> {
  String test = "";
  habilitateAuthentication() async {
    SharedPreferences oSharePrefs = await SharedPreferences.getInstance();
    bool? habilitate = oSharePrefs.getBool('habilitated');
    if (habilitate == null || habilitate == false) {
      habilitate = true;
    } else {
      habilitate = false;
    }
    await oSharePrefs.setBool('habilitated', habilitate);
    text();
  }

  // help to change text
  text() async {
    SharedPreferences oSharePrefs = await SharedPreferences.getInstance();
    bool? habilitate = oSharePrefs.getBool('habilitated');
    if (habilitate == null || habilitate == false) {
      setState(() {
        test = "Habilitar";
      });
    } else {
      setState(() {
        test = "Deshabilitar";
      });
    }
  }

  @override
  void initState() {
    super.initState();
    text();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Habilitation Page'),
              backgroundColor: Colors.lightBlue,
            ), //Builder para trabajar con el contexto
            body: Center(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
                child: Column(
                  children: [
                    const Text("Habilitar login con datos biométricos"),
                    const SizedBox(
                      height: 15,
                    ),
                    ElevatedButton(
                        style: ButtonStyle(
                            padding: MaterialStateProperty.all(
                                const EdgeInsets.fromLTRB(60, 0, 60, 0)),
                            backgroundColor:
                                const MaterialStatePropertyAll<Color>(
                                    Colors.lightBlue),
                            shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                    side: const BorderSide(
                                        color: Colors.lightBlue)))),
                        onPressed: () {
                          habilitateAuthentication();
                        },
                        child: Text(
                          test,
                          style: const TextStyle(fontSize: 20),
                        )),
                    ElevatedButton(
                        style: ButtonStyle(
                            padding: MaterialStateProperty.all(
                                const EdgeInsets.fromLTRB(60, 0, 60, 0)),
                            backgroundColor:
                                const MaterialStatePropertyAll<Color>(
                                    Colors.lightBlue),
                            shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                    side: const BorderSide(
                                        color: Colors.lightBlue)))),
                        onPressed: () {
                          Get.to(() => const MyApp());
                        },
                        child: const Text(
                          'Log Out',
                          style: TextStyle(fontSize: 20),
                        )),
                  ],
                ),
              ),
            )));
  }
}
