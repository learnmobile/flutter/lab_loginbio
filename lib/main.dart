import 'package:flutter/material.dart';
import 'local_auth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:get/get.dart';
import 'package:lab_loginbio/habilitate.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() {
  runApp(const GetMaterialApp(
    title: 'Login',
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // Create storage
  final _storage = const FlutterSecureStorage();
  final String keyMail = "KEY_MAIL";
  final String keyPass = "KEY_PASS";

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  bool test = false;

  isActiveAuthentication() async {
    SharedPreferences oSharePrefs = await SharedPreferences.getInstance();
    bool? isActivate = oSharePrefs.getBool('habilitated');
    if (isActivate == null || isActivate == false) {
      setState(() {
        test = false;
      });
    } else {
      setState(() {
        test = true;
      });
    }
  }

  fLogin(context) async {
    http.Response response =
        await http.post(Uri.parse('http://192.168.1.8:3000/login'),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonEncode(<String, String>{
              'username': usernameController.text,
              'password': passwordController.text,
            }));
    if (response.statusCode == 200) {
      // Write data
      await _storage.write(key: keyMail, value: usernameController.text);
      await _storage.write(key: keyPass, value: passwordController.text);
      Get.to(() => const Habilitation());
    } else {
      Get.showSnackbar(
        const GetSnackBar(
          message: 'Username and Password Invalid',
          icon: Icon(Icons.password),
          duration: Duration(seconds: 3),
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    isActiveAuthentication();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Login Page'),
              backgroundColor: Colors.lightBlue,
            ), //Builder para trabajar con el contexto
            body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
              child: Column(
                children: [
                  const FlutterLogo(size: 100),
                  TextField(
                      controller: usernameController,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: "Email",
                      )),
                  TextField(
                      controller: passwordController,
                      obscureText: true,
                      enableSuggestions: false,
                      autocorrect: false,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(), labelText: "Password")),
                  ElevatedButton(
                      style: ButtonStyle(
                          padding: MaterialStateProperty.all(
                              const EdgeInsets.fromLTRB(60, 0, 60, 0)),
                          backgroundColor:
                              const MaterialStatePropertyAll<Color>(
                                  Colors.lightBlue),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: const BorderSide(
                                          color: Colors.lightBlue)))),
                      onPressed: () {
                        fLogin(context);
                      },
                      child: const Text(
                        'Login',
                        style: TextStyle(fontSize: 20),
                      )),
                  /* ElevatedButton(
                      onPressed: () {
                        isActiveAuthentication();
                      },
                      child: Text("Testing")), */
                  if (test)
                    ElevatedButton(
                        style: ButtonStyle(
                            padding: MaterialStateProperty.all(
                                const EdgeInsets.fromLTRB(30, 10, 30, 10)),
                            backgroundColor:
                                const MaterialStatePropertyAll<Color>(
                                    Colors.lightBlue),
                            shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                    side: const BorderSide(
                                        color: Colors.lightBlue)))),
                        onPressed: () async {
                          final didAuthenticate =
                              await LocalAuth.didAuthenticate();
                          if (didAuthenticate) {
                            usernameController.text =
                                await _storage.read(key: keyMail) ?? '';
                            passwordController.text =
                                await _storage.read(key: keyPass) ?? '';
                          }
                          fLogin(context);
                        },
                        child: const Text(
                          'Iniciar Sesión con Datos Biométricos',
                          style: TextStyle(fontSize: 20),
                        )),
                ],
              ),
            )));
  }
}
